package com.nabbel.examples.modelitems;

import java.text.DecimalFormat;


public class WeatherData {
	
	DecimalFormat temperature_format = new DecimalFormat("#.#");
	
	private final static float NO_VALUE = (float) -1.0;
	private final static String NO_VALUE_STRING = "_";

	public String poblacion = "";
	public String provincia = "";
	public String pred_desc = "";
	public byte pred_code = Byte.MIN_VALUE;
	public float temp_min = NO_VALUE;
	public float temp_max = NO_VALUE;
	
	public String getTempMinAsString(){
		if (temp_min == NO_VALUE){
			return NO_VALUE_STRING;
		}
		return temperature_format.format(temp_min);
	}
	
	public String getTempMaxAsString(){
		if (temp_max == NO_VALUE){
			return NO_VALUE_STRING;
		}
		return temperature_format.format(temp_max);
	}

	public boolean hasBeenRead() {
		return  (poblacion.length() > 0) && (temp_min != NO_VALUE)
				&& (temp_max != NO_VALUE);
	}

	public boolean hasPredictionDescription() {
		return pred_desc.length()>0;
	}

	public boolean hasPredictionCode() {
		return pred_code!=Byte.MIN_VALUE;
	}

	
	private final static String IMAGE_BASE = "http://www.aemet.es/imagenes/gif/estado_cielo/";
	private final static String IMAGE_EXTENSION = ".gif";
	
	public String getImageURL() {
		return IMAGE_BASE+String.valueOf(pred_code)+IMAGE_EXTENSION;
	}
	
}
