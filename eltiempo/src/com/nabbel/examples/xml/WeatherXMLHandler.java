package com.nabbel.examples.xml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.xml.sax.Attributes;

import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;

import com.nabbel.examples.modelitems.WeatherData;

public class WeatherXMLHandler{
	
	private static WeatherXMLHandler instance;
	protected boolean reading_prediction = false;

	private WeatherXMLHandler() {
	}

	public static WeatherXMLHandler getInstance() {
		if (null == instance) {
			instance = new WeatherXMLHandler();
		}
		return instance;
	}
	
	public WeatherData getParsedItem(String xml_url) throws Exception{
	
		WeatherData parsed_item = new WeatherData();		
		InputStream stream_to_parse = getStreamFromStringURL(xml_url);
		RootElement root = new RootElement("root");
		defineResourceListeners(root, parsed_item);
		
		Xml.parse(stream_to_parse, Xml.Encoding.ISO_8859_1, root.getContentHandler());
		
		return parsed_item;
		
	}
	

	
	private void defineResourceListeners(RootElement root,
			final WeatherData parsed_item) {

		root.getChild("nombre").setEndTextElementListener(new EndTextElementListener(){

			public void end(String body) {
				parsed_item.poblacion = body;
			}
			
		});
		
		root.getChild("provincia").setEndTextElementListener(new EndTextElementListener(){

			public void end(String body) {
				parsed_item.provincia = body;
			}
			
		});
		
		Element dia = root.getChild("prediccion").getChild("dia");
		
		Element temperatura = dia.getChild("temperatura");
		temperatura.getChild("maxima").setEndTextElementListener(new EndTextElementListener() {
			
			public void end(String body) {
				if (!parsed_item.hasBeenRead()){
					parsed_item.temp_max = Float.parseFloat(body);
				} 
			}
		});
		temperatura.getChild("minima").setEndTextElementListener(new EndTextElementListener() {
			
			public void end(String body) {
				if (!parsed_item.hasBeenRead()){
					parsed_item.temp_min = Float.parseFloat(body);
				}
			}
		});
		
		Element estado_cielo = dia.getChild("estado_cielo");
		estado_cielo.setEndElementListener(new EndElementListener(){

			public void end() {
				
			}
			
			
		});
		
		
		
		estado_cielo.setStartElementListener(new StartElementListener(){

			public void start(Attributes attributes) {
				if (parsed_item.hasPredictionDescription()){
					return;
				}
				
				String periodo = attributes.getValue("periodo");
				if (periodo.equals("12-18")){
					reading_prediction  = true;
					parsed_item.pred_desc = attributes.getValue("descripcion");
				}
			}			
		});
		
		estado_cielo.setEndTextElementListener(new EndTextElementListener() {
			
			public void end(String body) {
				if (parsed_item.hasPredictionCode()){
					return;
				}
				
				if (reading_prediction){
					parsed_item.pred_code = Byte.parseByte(body);
				}
				
			}
		});
		
		
	}

	private InputStream getStreamFromStringURL(String xml_url) throws IOException{
		URL url_to_parse = new URL(xml_url);
		return url_to_parse.openConnection().getInputStream();
		
	}

}
