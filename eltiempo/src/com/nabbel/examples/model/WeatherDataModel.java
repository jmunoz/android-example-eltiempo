package com.nabbel.examples.model;

import java.util.HashMap;
import java.util.Map;

import com.nabbel.examples.modelitems.WeatherData;
import com.nabbel.examples.xml.WeatherXMLHandler;

public class WeatherDataModel {
	
	private final static String XML_PATH_BASE = "http://www.aemet.es/xml/municipios/localidad_";
	private final static String XML_PATH_BASE_EXTENSION = ".xml";
	
	
	private static WeatherDataModel instance;

	private WeatherDataModel() {
	}

	public static WeatherDataModel getInstance() {
		if (null == instance) {
			instance = new WeatherDataModel();
		}
		return instance;
	}
	
	private Map<String, WeatherData> cache = new HashMap<String, WeatherData>();
	
	public WeatherData getWeatherDataForLocationId(String location_id) throws Exception{
		if (cache.containsKey(location_id)){
			return cache.get(location_id);
		}
		
		String xml_url = getXMLURLForLocationId(location_id);
		WeatherData data =  WeatherXMLHandler.getInstance().getParsedItem(xml_url);
		cache.put(location_id, data);
		return data;
	}

	private String getXMLURLForLocationId(String location_id) {
		return XML_PATH_BASE+location_id+XML_PATH_BASE_EXTENSION;
	}
	
//	public void refreshData(){
//		for (String location : cache.keySet()){
//			
//		}
//	}
	
//	public List<WeatherData> getAllWeatherData(){
//		
//	}

}
