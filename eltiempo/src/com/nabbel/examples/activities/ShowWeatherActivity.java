package com.nabbel.examples.activities;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nabbel.examples.R;
import com.nabbel.examples.model.WeatherDataModel;
import com.nabbel.examples.modelitems.WeatherData;

public class ShowWeatherActivity extends Activity {
    /** Called when the activity is first created. */
	

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_weather);
                
        configureSpinner();
        
          
    }
    
	private void loadLocationWeatherData(String location_id){
		  try {
				WeatherData data = WeatherDataModel.getInstance().getWeatherDataForLocationId(location_id);
				updateDataInScreen(data);   
	      
	      } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}    
	}

	private void configureSpinner() {
		Spinner spinner = (Spinner) findViewById(R.id.spinner);	
		spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getWeatherLocations()));
		spinner.setOnItemSelectedListener(new OnItemSelectedListener(){

			public void onItemSelected(AdapterView<?> theadapter, View arg1,
					int arg2, long arg3) {
				String selected_item = (String) theadapter.getSelectedItem();
				String location_id = spinner_data.get(selected_item);
				loadLocationWeatherData(location_id);
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		spinner.setSelection(0);
	}
	
	private static List<String> getWeatherLocations() {
		initSpinnerData();
		return new ArrayList<String>(spinner_data.keySet());
	}

	private static Map<String,String> spinner_data = new HashMap<String,String>();

	private static void initSpinnerData(){
		spinner_data.put("Castellón", "12040");
		spinner_data.put("Vila-real","12135");
		spinner_data.put("Madrid","28079");
		spinner_data.put("Burgos", "09059");
	}

	private void updateDataInScreen(WeatherData data) {
		TextView poblacion = (TextView) findViewById(R.id.poblacion);
		poblacion.setText(data.poblacion);
		
		TextView provincia = (TextView) findViewById(R.id.provincia);
		provincia.setText(data.provincia);
		
		TextView temp_max = (TextView) findViewById(R.id.temp_max);
		temp_max.setText(data.getTempMaxAsString());
		
		TextView temp_min = (TextView) findViewById(R.id.temp_min);
		temp_min.setText(data.getTempMinAsString());
		
		TextView prediccion_text = (TextView) findViewById(R.id.prediccion_text);
		prediccion_text.setText(data.pred_desc);
		
		final ImageView image = (ImageView) findViewById(R.id.prediccion_image);
		image.setVisibility(View.INVISIBLE);
		new FetchImageTask() { 
	        protected void onPostExecute(Bitmap result) {
	            if (result != null) {
	            	image.setImageBitmap(result);
	            	image.setVisibility(View.VISIBLE);
	            }
	        }
	    }.execute(data.getImageURL());
		
	}
	
	private class FetchImageTask extends AsyncTask<String, Integer, Bitmap> {
	    @Override
	    protected Bitmap doInBackground(String... arg0) {
	    	Bitmap b = null;
	    	try {
				 b = BitmapFactory.decodeStream((InputStream) new URL(arg0[0]).getContent());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
	        return b;
	    }	
	}
	
	

}